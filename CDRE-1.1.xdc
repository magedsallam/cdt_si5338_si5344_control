set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 66 [current_design]
set_property CONFIG_VOLTAGE 3.3 [current_design]
set_property CFGBVS VCCO [current_design]
set_property CONFIG_MODE SPIx4 [current_design]
set_property BITSTREAM.CONFIG.SPI_32BIT_ADDR YES [current_design]
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]
set_property BITSTREAM.CONFIG.UNUSEDPIN PULLDOWN [current_design]


#############
### Infos ###
#############
# The LVDS_25 I/O standard is only available in the HighRange I/O banks. Artix-7 has only HighRange I/O banks.
# The CDRE board uses one TE 712 board from Trenz for the central FPGA part of CDRE (see TRM-TE0712-02.pdf).



##############
### Clocks ###
##############

set_property PACKAGE_PIN F6 [get_ports MGT_CLK_P]
set_property PACKAGE_PIN E6 [get_ports MGT_CLK_N]

#set_property PACKAGE_PIN	F10		[get_ports MGT_CLK_P];	# for future setup through jitter cleaner Si5344: 125 MHz GTP tranceiver clock on bank 216
#set_property PACKAGE_PIN	E10		[get_ports MGT_CLK_N];	# internally terminated with 100 Ohm (see ug476_7_series_transceivers, p. 33)

set_property IOSTANDARD DIFF_SSTL15 [get_ports CLK50MHZ_P]
set_property PACKAGE_PIN H4 [get_ports CLK50MHZ_P]
set_property PACKAGE_PIN G4 [get_ports CLK50MHZ_N]
set_property IOSTANDARD DIFF_SSTL15 [get_ports CLK50MHZ_N]
set_property PACKAGE_PIN W11 [get_ports CLK10MHz]
set_property IOSTANDARD LVCMOS25 [get_ports CLK10MHz]
#LVCMOS33
create_clock -period 8.000 -name mgt_clk [get_ports MGT_CLK_P]
create_clock -period 20.000 -name CLK50MHZ [get_ports CLK50MHZ_P]
create_clock -period 100.000 -name CLK10MHZ [get_ports CLK10MHz]
set_clock_groups -asynchronous -group [get_clocks clk_out1_clk_wiz_0] -group [get_clocks mgt_clk]
set_clock_groups -asynchronous -group [get_clocks clk_out1_clk_wiz_0] -group [get_clocks CLK10MHZ]

set_clock_groups -asynchronous -group [get_clocks clk_out1_clk_wiz_0] -group [get_clocks CLK50MHZ]
set_clock_groups -asynchronous -group [get_clocks CLK50MHZ] -group [get_clocks mgt_clk]
set_clock_groups -asynchronous -group [get_clocks CLK50MHZ] -group [get_clocks CLK10MHZ]
set_clock_groups -asynchronous -group [get_clocks CLK50MHZ] -group [get_clocks mgt_clk]

set_clock_groups -asynchronous -group [get_clocks mgt_clk] -group [get_clocks CLK10MHZ]
set_clock_groups -asynchronous -group [get_clocks mgt_clk] -group [get_clocks CLK50MHZ]
set_clock_groups -asynchronous -group [get_clocks mgt_clk] -group [get_clocks clk_out1_clk_wiz_0]
set_clock_groups -asynchronous -group [get_clocks CLK10MHZ] -group [get_clocks mgt_clk]
set_clock_groups -asynchronous -group [get_clocks CLK10MHZ] -group [get_clocks clk_out1_clk_wiz_0]
set_clock_groups -asynchronous -group [get_clocks CLK10MHZ] -group [get_clocks CLK50MHZ]





#############
### SFP's ### #checked
#############
# SPI interface to SFP
#set_property PACKAGE_PIN			[get_ports SFP_MOD_DEF0];	# SFP_MOD-DEF0 is not routed on CDRE board to FPGA; instead SFP_MOD-DEF1
#set_property PACKAGE_PIN	R18		[get_ports SFP_MOD_DEF1];	# addresses both SFP's simultaneously on CDRE board.
#set_property IOSTANDARD LVCMOS33	[get_ports SFP_MOD_DEF1];	# maybe it is necessary to increase DRIVE here due to simultanous connection to both SFP's.
#set_property PACKAGE_PIN	R19		[get_ports SFP0_MOD_DEF2];	# SPI response from SFP0
#set_property IOSTANDARD LVCMOS33	[get_ports SFP0_MOD_DEF2]
#set_property PACKAGE_PIN	T18		[get_ports SFP1_MOD_DEF2];	# SPI response from SFP1
#set_property IOSTANDARD LVCMOS33	[get_ports SFP1_MOD_DEF2]
## SFP_RATE_SELECT is connected too high (high rate) on CDRE board
## SFP0 #checked
#set_property PACKAGE_PIN	B8		[get_ports SFP0_RX_P]
#set_property PACKAGE_PIN	A8		[get_ports SFP0_RX_N]
#set_property PACKAGE_PIN	B4		[get_ports SFP0_TX_P]
#set_property PACKAGE_PIN	A4		[get_ports SFP0_TX_N]
# SFP1 #checked
#set_property PACKAGE_PIN	D11		[get_ports SFP1_RX_P]
#set_property PACKAGE_PIN	C11		[get_ports SFP1_RX_N]
#set_property PACKAGE_PIN	D5		[get_ports SFP1_TX_P]
#set_property PACKAGE_PIN	C5		[get_ports SFP1_TX_N]
# SFP_TX_FAULT is not routed on CDRE board
# SFP_TX_DIS is grounded on CDRE board
# SFP_LOS is routed to LED D2 on CDRE board



##############
### Others ###
##############
set_property PACKAGE_PIN P19 [get_ports LED_O]
set_property IOSTANDARD LVCMOS33 [get_ports LED_O]
#set_property PACKAGE_PIN	P16		[get_ports MIO14]
#set_property PACKAGE_PIN	U18		[get_ports MIO15]
## interrupt
#set_property PACKAGE_PIN 	B2  	[get_ports {pll_int_io}]
#set_property IOSTANDARD LVCMOS33 	[get_ports {pll_int_i}]


###################################
### Si5338 Quad Clock Generator ###
###################################
set_property PACKAGE_PIN T20 [get_ports sda]
set_property IOSTANDARD LVCMOS33 [get_ports sda]
set_property PACKAGE_PIN W21 [get_ports scl]
set_property IOSTANDARD LVCMOS33 [get_ports scl]
## Si5338 output CLK0: disabled via Trenz firmware
#set_property PACKAGE_PIN	K4		[get_ports clk0_p_i]
#set_property IOSTANDARD DIFF_SSTL15	[get_ports clk0_p_i]
#set_property PACKAGE_PIN	J4		[get_ports clk0_n_i]
#set_property IOSTANDARD DIFF_SSTL15	[get_ports clk0_n_i]
## Si5338 output CLK1B: disabled via Trenz firmware
#set_property PACKAGE_PIN	R4		[get_ports clk1_i]
#set_property IOSTANDARD LVCMOS33	[get_ports clk1_i]
## si5338 output CLK3 => PLL_CLK on FPGA (see block 'Clocks' above)



################################################
### Jitter Cleaner Chip Si5344 on CDRE board ### #checked
################################################
set_property IOSTANDARD LVDS_25 [get_ports JC_in_clk_p]
set_property PACKAGE_PIN V10 [get_ports JC_in_clk_p]
set_property PACKAGE_PIN W10 [get_ports JC_in_clk_n]
set_property IOSTANDARD LVDS_25 [get_ports JC_in_clk_n]
#set_property PACKAGE_PIN	T16		[get_ports JC_IN1_P];		# Clock input 1
#set_property IOSTANDARD LVDS_25	[get_ports JC_IN1_P]
#set_property PACKAGE_PIN	U16		[get_ports JC_IN1_N]
#set_property IOSTANDARD LVDS_25	[get_ports JC_IN1_N]
#set_property IOSTANDARD LVDS_25	[get_ports JC_out_clk_n]
set_property PACKAGE_PIN F10 [get_ports JC_out_clk_p]
set_property PACKAGE_PIN E10 [get_ports JC_out_clk_n]
#set_property IOSTANDARD LVDS_25	[get_ports JC_out_clk_p]
#set_property PACKAGE_PIN	V13		[get_ports JC_OUT1_CLK_P];
#set_property IOSTANDARD LVDS_25	[get_ports JC_OUT1_CLK_P]
#set_property PACKAGE_PIN	V14		[get_ports JC_OUT1_CLK_N]
#set_property IOSTANDARD LVDS_25	[get_ports JC_OUT1_CLK_N]
## Clock output 2 goes to Si5338 input CLKIn2
## Clock output 3 is fed back to input3 for zero delay feedback mode
set_property PACKAGE_PIN AA19 [get_ports JC_scl]
set_property IOSTANDARD LVCMOS33 [get_ports JC_scl]
set_property PULLUP true [get_ports JC_scl]
set_property PACKAGE_PIN AB20 [get_ports JC_sda]
set_property IOSTANDARD LVCMOS33 [get_ports JC_sda]
set_property PULLUP true [get_ports JC_sda]
set_property PACKAGE_PIN J16 [get_ports JC_A1]
set_property IOSTANDARD LVCMOS33 [get_ports JC_A1]
set_property PACKAGE_PIN U17 [get_ports JC_reset_N]
set_property IOSTANDARD LVCMOS33 [get_ports JC_reset_N]
#set_property PACKAGE_PIN	R17		[get_ports JC_\LOL];		# Jitter Cleaner Los-Of-Lock Indicator (routed to JB1 and LED Yellow on RJ45)
#set_property IOSTANDARD LVCMOS33	[get_ports {JC_\LOL}]



#######################################################
### Ethernet PHY chip (see TRM-TE0712-02.pdf, p.10) ###
#######################################################
## All signals are connected to the FPGA bank 14 and correspond to LVCMOS33 standard.
#set_property PACKAGE_PIN	N17		[get_ports phy_RST];			# 'ETH-RST' at Trenz
#set_property IOSTANDARD LVCMOS33	[get_ports phy_RST]
#set_property PACKAGE_PIN	N15		[get_ports phy_Link_LED];		# 'LINK_LED' at Trenz; indicater signal from PHY
#set_property IOSTANDARD LVCMOS33	[get_ports phy_Link_LED]
#set_property PACKAGE_PIN	R16		[get_ports phy_mdc];			# 'MDC' at Trenz
#set_property IOSTANDARD LVCMOS33	[get_ports phy_mdc]
#set_property PULLUP true			[get_ports phy_mdc];
#set_property PACKAGE_PIN	P17		[get_ports phy_mdio];			# 'MDIO' at Trenz
#set_property IOSTANDARD LVCMOS33	[get_ports phy_mdio]
#set_property PULLUP true			[get_ports phy_mdio]
#set_property PACKAGE_PIN	P14		[get_ports {rmii2phy_txd[0]}];	# 'ETH-TX_D0' at Trenz
#set_property PACKAGE_PIN	P15		[get_ports {rmii2phy_txd[1]}];	# 'ETH-TX_D1' at Trenz
#set_property IOSTANDARD LVCMOS33	[get_ports {rmii2phy_txd[*]}]
#set_property PACKAGE_PIN	R14		[get_ports rmii2phy_tx_en];		# 'ETH-TX_EN' at Trenz
#set_property IOSTANDARD LVCMOS33	[get_ports rmii2phy_tx_en]
#set_property PACKAGE_PIN	N13		[get_ports {phy2rmii_rxd[0]}];	# 'ETH-RX_D0' at Trenz
#set_property PACKAGE_PIN	N14		[get_ports {phy2rmii_rxd[1]}];	# 'ETH-RX_D1' at Trenz
#set_property IOSTANDARD LVCMOS33	[get_ports {phy2rmii_rxd[*]}]
#set_property PACKAGE_PIN	P20		[get_ports phy2rmii_crs_dv];	# 'ETH-RX_DV' at Trenz
#set_property IOSTANDARD LVCMOS33	[get_ports phy2rmii_crs_dv]



################################################################
#### inner two LVDS pairs (not used by Ethernet PHY) at RJ45 ###	#checked
################################################################
## These signals are routed on the CDRE board.
#set_property PACKAGE_PIN 	AA15	[get_ports AUX_IO_A_P]
#set_property IOSTANDARD LVDS_25	[get_ports AUX_IO_A_P]
#set_property PACKAGE_PIN 	AB15	[get_ports AUX_IO_A_N]
#set_property IOSTANDARD LVDS_25	[get_ports AUX_IO_A_N]
#set_property PACKAGE_PIN 	AB17	[get_ports AUX_IO_B_P]
#set_property IOSTANDARD LVDS_25	[get_ports AUX_IO_B_P]
#set_property PACKAGE_PIN 	AB16	[get_ports AUX_IO_B_N]
#set_property IOSTANDARD LVDS_25	[get_ports AUX_IO_B_N]



######################################
### Avago Versatile Link Interface ### #checked
######################################
#set_property PACKAGE_PIN	W19		[get_ports VLINK_TX]
#set_property IOSTANDARD LVCMOS33	[get_ports VLINK_TX]
#set_property PACKAGE_PIN	W20		[get_ports VLINK_RX]
#set_property IOSTANDARD LVCMOS33	[get_ports VLINK_RX]
#set_property PACKAGE_PIN	V18		[get_ports VLINK2_TX]
#set_property IOSTANDARD LVCMOS33	[get_ports VLINK2_TX]
#set_property PACKAGE_PIN	V19		[get_ports VLINK2_RX]
#set_property IOSTANDARD LVCMOS33	[get_ports VLINK2_RX]



###########################
### ASIC Clients 0 to 7 ###
###########################
## fanned-out CLK signals
#set_property PACKAGE_PIN	W15		[get_ports RCLK_P]
#set_property IOSTANDARD LVDS_25		[get_ports RCLK_P]
#set_property PACKAGE_PIN	W16		[get_ports RCLK_N]
#set_property IOSTANDARD LVDS_25		[get_ports RCLK_N]
#set_property PACKAGE_PIN	Y16		[get_ports SCLK_P]
#set_property IOSTANDARD LVDS_25		[get_ports SCLK_P]
#set_property PACKAGE_PIN	AA16	[get_ports SCLK_N]
#set_property IOSTANDARD LVDS_25		[get_ports SCLK_N]

## special PT2 signals
#set_property PACKAGE_PIN W12 		[get_ports PT2_PDWN]
#set_property IOSTANDARD LVCMOS33	[get_ports {PT2_PDWN}]
#set_property PACKAGE_PIN AA21 		[get_ports PT2_SC-CLK]
#set_property IOSTANDARD LVCMOS33	[get_ports {PT2_SC-CLK}]
#set_property PACKAGE_PIN AA20 		[get_ports PT2_INJDIG-0]
#set_property IOSTANDARD LVCMOS33	[get_ports {PT2_INJDIG-0}]
#set_property PACKAGE_PIN AB18 		[get_ports PT2_INJDIG-1]
#set_property IOSTANDARD LVCMOS33	[get_ports {PT2_INJDIG-1}]
#set_property PACKAGE_PIN	AA18	[get_ports PT2_INJAUX]
#set_property IOSTANDARD LVCMOS33	[get_ports {PT2_INJAUX}]
#set_property PACKAGE_PIN	T14		[get_ports PT2_RO-SAMPLE_P]
#set_property IOSTANDARD LVDS_25	[get_ports PT2_RO-SAMPLE_P]
#set_property PACKAGE_PIN	T15		[get_ports PT2_RO-SAMPLE_N]
#set_property IOSTANDARD LVDS_25	[get_ports PT2_RO-SAMPLE_N]

## one to one signals
## ASIC 0
#set_property PACKAGE_PIN M18 		[get_ports ASIC0_\RST_P]
#set_property IOSTANDARD LVCMOS25 	[get_ports {ASIC0_\RST_P}]
#set_property PACKAGE_PIN L18 		[get_ports ASIC0_\RST_N]
#set_property IOSTANDARD LVCMOS25 	[get_ports {ASIC0_\RST_N}]
#set_property PACKAGE_PIN U20 		[get_ports ASIC0_SDA]
#set_property IOSTANDARD LVCMOS33 	[get_ports {ASIC0_SDA}]
#set_property PACKAGE_PIN V20 		[get_ports ASIC0_SCL]
#set_property IOSTANDARD LVCMOS33 	[get_ports {ASIC0_SCL}]

#set_property PACKAGE_PIN N19 		[get_ports ASIC0_FRAME_N]
#set_property PACKAGE_PIN N18 		[get_ports ASIC0_FRAME_P]
#set_property PACKAGE_PIN L16 		[get_ports ASIC0_DO_P]
#set_property IOSTANDARD LVDS_25 	[get_ports {ASIC0_D0_P}]
#set_property IOSTANDARD DIFF_TERM = TRUE 	[get_ports {ASIC0_D0_P}]
#set_property PACKAGE_PIN K16 		[get_ports ASIC0_DO_N]
#set_property PACKAGE_PIN K13 		[get_ports ASIC0_D1_P]
#set_property PACKAGE_PIN K14 		[get_ports ASIC0_D1_N]
#set_property PACKAGE_PIN J14 		[get_ports ASIC0_D2_P]
#set_property PACKAGE_PIN H14 		[get_ports ASIC0_D2_N]

## ASIC 1
#set_property PACKAGE_PIN N22 		[get_ports ASIC1_\RST_P]
#set_property IOSTANDARD LVCMOS25 	[get_ports {ASIC1_\RST_P}]
#set_property PACKAGE_PIN M22 		[get_ports ASIC1_\RST_N]
#set_property IOSTANDARD LVCMOS25 	[get_ports {ASIC1_\RST_N}]
#set_property PACKAGE_PIN T21 		[get_ports ASIC1_SCL]
#set_property IOSTANDARD LVCMOS33 	[get_ports {ASIC1_SCL}]
#set_property PACKAGE_PIN U21 		[get_ports ASIC1_SDA]
#set_property IOSTANDARD LVCMOS33 	[get_ports {ASIC1_SDA}]

#set_property PACKAGE_PIN L15 		[get_ports ASIC1_FRAME_N]
#set_property PACKAGE_PIN L14 		[get_ports ASIC1_FRAME_P]
#set_property PACKAGE_PIN M13 		[get_ports ASIC1_D0_P]
#set_property PACKAGE_PIN L13 		[get_ports ASIC1_D0_N]
#set_property PACKAGE_PIN K17 		[get_ports ASIC1_D1_P]
#set_property PACKAGE_PIN J17 		[get_ports ASIC1_D1_N]
#set_property PACKAGE_PIN M16 		[get_ports ASIC1_D2_N]
#set_property PACKAGE_PIN M15 		[get_ports ASIC1_D2_P]

## ASIC 2
#set_property PACKAGE_PIN M21 		[get_ports ASIC2_\RST_P]
#set_property IOSTANDARD LVCMOS25 	[get_ports {ASIC2_\RST_P}]
#set_property PACKAGE_PIN L21 		[get_ports ASIC2_\RST_N]
#set_property IOSTANDARD LVCMOS25 	[get_ports {ASIC2_\RST_N}]
#set_property PACKAGE_PIN Y21 		[get_ports ASIC2_SCL]
#set_property IOSTANDARD LVCMOS33 	[get_ports {ASIC2_SCL}]
#set_property PACKAGE_PIN Y22 		[get_ports ASIC2_SDA]
#set_property IOSTANDARD LVCMOS33 	[get_ports {ASIC2_SDA}]

#set_property PACKAGE_PIN B21 		[get_ports ASIC2_FRAME_P]
#set_property PACKAGE_PIN A21 		[get_ports ASIC2_FRAME_N]
#set_property PACKAGE_PIN E22 		[get_ports ASIC2_D0_P]
#set_property PACKAGE_PIN D22 		[get_ports ASIC2_D0_N]
#set_property PACKAGE_PIN E21 		[get_ports ASIC2_D1_P]
#set_property PACKAGE_PIN D21 		[get_ports ASIC2_D1_N]
#set_property PACKAGE_PIN G22 		[get_ports ASIC2_D2_N]
#set_property PACKAGE_PIN G21 		[get_ports ASIC2_D2_P]

## ASIC 3
#set_property PACKAGE_PIN AA10 		[get_ports ASIC3_\RST_P]
#set_property IOSTANDARD LVCMOS25 	[get_ports {ASIC3_\RST_P}]
#set_property PACKAGE_PIN AA11 		[get_ports ASIC3_\RST_N]
#set_property IOSTANDARD LVCMOS25 	[get_ports {ASIC3_\RST_N}]
#set_property PACKAGE_PIN U15 		[get_ports ASIC3_SCL]
#set_property IOSTANDARD LVCMOS25 	[get_ports {ASIC3_SCL}]
#set_property PACKAGE_PIN V15 		[get_ports ASIC3_SDA]
#set_property IOSTANDARD LVCMOS25 	[get_ports {ASIC3_SDA}]

#set_property PACKAGE_PIN C18 		[get_ports ASIC3_FRAME_P]
#set_property PACKAGE_PIN C19 		[get_ports ASIC3_FRAME_N]
#set_property PACKAGE_PIN E19 		[get_ports ASIC3_D0_P]
#set_property PACKAGE_PIN D19 		[get_ports ASIC3_DO_N]
#set_property PACKAGE_PIN F18 		[get_ports ASIC3_D1_P]
#set_property PACKAGE_PIN E18 		[get_ports ASIC3_D1_N]
#set_property PACKAGE_PIN B20 		[get_ports ASIC3_D2_P]
#set_property PACKAGE_PIN A20 		[get_ports ASIC3_D2_N]

## ASIC 4
#set_property PACKAGE_PIN Y13 		[get_ports ASIC4_\RST_P]
#set_property IOSTANDARD LVCMOS25 	[get_ports {ASIC4_\RST_P}]
#set_property PACKAGE_PIN AA14 		[get_ports ASIC4_\RST_N]
#set_property IOSTANDARD LVCMOS25 	[get_ports {ASIC4_\RST_N}]
#set_property PACKAGE_PIN AB22 		[get_ports ASIC4_SDA]
#set_property IOSTANDARD LVCMOS33 	[get_ports {ASIC4_SDA}]
#set_property PACKAGE_PIN AB21 		[get_ports ASIC4_SCL]
#set_property IOSTANDARD LVCMOS33 	[get_ports {ASIC4_SCL}]

#set_property PACKAGE_PIN A19 		[get_ports ASIC4_FRAME_N]
#set_property PACKAGE_PIN A18 		[get_ports ASIC4_FRAME_P]
#set_property PACKAGE_PIN F20 		[get_ports ASIC4_D0_N]
#set_property PACKAGE_PIN F19 		[get_ports ASIC4_D0_P]
#set_property PACKAGE_PIN C20 		[get_ports ASIC4_D1_N]
#set_property PACKAGE_PIN D20 		[get_ports ASIC4_D1_P]
#set_property PACKAGE_PIN C22 		[get_ports ASIC4_D2_P]
#set_property PACKAGE_PIN B22 		[get_ports ASIC4_D2_N]

## ASIC 5
#set_property PACKAGE_PIN Y11 		[get_ports ASIC5_\RST_P]
#set_property IOSTANDARD LVCMOS25 	[get_ports {ASIC5_\RST_P}]
#set_property PACKAGE_PIN Y12 		[get_ports ASIC5_\RST_N]
#set_property IOSTANDARD LVCMOS25 	[get_ports {ASIC5_\RST_N}]
#set_property PACKAGE_PIN L19 		[get_ports ASIC5_SCL]
#set_property IOSTANDARD LVCMOS25 	[get_ports {ASIC5_SCL}]
#set_property PACKAGE_PIN L20 		[get_ports ASIC5_SDA]
#set_property IOSTANDARD LVCMOS25 	[get_ports {ASIC5_SDA}]

#set_property PACKAGE_PIN D17 		[get_ports ASIC5_FRAME_P]
#set_property PACKAGE_PIN C17 		[get_ports ASIC5_FRAME_N]
#set_property PACKAGE_PIN A16 		[get_ports ASIC5_D0_N]
#set_property PACKAGE_PIN A15 		[get_ports ASIC5_D0_P]
#set_property PACKAGE_PIN A14 		[get_ports ASIC5_D1_N]
#set_property PACKAGE_PIN A13 		[get_ports ASIC5_D1_P]
#set_property PACKAGE_PIN B17 		[get_ports ASIC5_D2_P]
#set_property PACKAGE_PIN B18 		[get_ports ASIC5_D2_N]

## ASIC 6
#set_property PACKAGE_PIN W14 		[get_ports ASIC6_\RST_P]
#set_property IOSTANDARD LVCMOS25 	[get_ports {ASIC6_\RST_P}]
#set_property PACKAGE_PIN Y14 		[get_ports ASIC6_\RST_N]
#set_property IOSTANDARD LVCMOS25 	[get_ports {ASIC6_\RST_N}]
#set_property PACKAGE_PIN V17 		[get_ports ASIC6_SCL]
#set_property IOSTANDARD LVCMOS33 	[get_ports {ASIC6_SCL}]
#set_property PACKAGE_PIN W17 		[get_ports ASIC6_SDA]
#set_property IOSTANDARD LVCMOS33 	[get_ports {ASIC6_SDA}]

#set_property PACKAGE_PIN D16 		[get_ports ASIC6_FRAME_N]
#set_property PACKAGE_PIN E16 		[get_ports ASIC6_FRAME_P]
#set_property PACKAGE_PIN D15 		[get_ports ASIC6_D0_N]
#set_property PACKAGE_PIN D14 		[get_ports ASIC6_D0_P]
#set_property PACKAGE_PIN B16 		[get_ports ASIC6_D1_N]
#set_property PACKAGE_PIN B15 		[get_ports ASIC6_D1_P]
#set_property PACKAGE_PIN B13 		[get_ports ASIC6_D2_N]
#set_property PACKAGE_PIN C13 		[get_ports ASIC6_D2_P]

## ASIC 7
#set_property PACKAGE_PIN AA13 		[get_ports ASIC7_\RST_P]
#set_property IOSTANDARD LVCMOS25 	[get_ports {ASIC7_\RST_P}]
#set_property PACKAGE_PIN AB13 		[get_ports ASIC7_\RST_N]
#set_property IOSTANDARD LVCMOS25 	[get_ports {ASIC7_\RST_N}]
#set_property PACKAGE_PIN Y18 		[get_ports ASIC7_SCL]
#set_property IOSTANDARD LVCMOS33 	[get_ports {ASIC7_SCL}]
#set_property PACKAGE_PIN Y19 		[get_ports ASIC7_SDA]
#set_property IOSTANDARD LVCMOS33 	[get_ports {ASIC7_SDA}]

#set_property PACKAGE_PIN C15 		[get_ports ASIC7_FRAME_P]
#set_property PACKAGE_PIN C14 		[get_ports ASIC7_FRAME_N]
#set_property PACKAGE_PIN F14 		[get_ports ASIC7_D0_N]
#set_property PACKAGE_PIN F13 		[get_ports ASIC7_D0_P]
#set_property PACKAGE_PIN E14 		[get_ports ASIC7_D1_N]
#set_property PACKAGE_PIN E13 		[get_ports ASIC7_D1_P]
#set_property PACKAGE_PIN E17 		[get_ports ASIC7_D2_N]
#set_property PACKAGE_PIN F16 		[get_ports ASIC7_D2_P]



#######################################
#### 8 channel pipeline ADC signals ###
#######################################
#set_property PACKAGE_PIN K22 		[get_ports ADC_CLK_N]
#set_property IOSTANDARD LVDS_25		[get_ports ADC_CLK_N]
#set_property PACKAGE_PIN K21 		[get_ports ADC_CLK_P]
#set_property IOSTANDARD LVDS_25		[get_ports ADC_CLK_P]
#set_property PACKAGE_PIN K18 		[get_ports ADC_SCLK/DTP]
#set_property IOSTANDARD LVCMOS25	[get_ports {ADC_SCLK/DTP}]
#set_property PACKAGE_PIN K19 		[get_ports ADC_SDIO/ODM]
#set_property IOSTANDARD LVCMOS25	[get_ports {ADC_SDIO/ODM}]
#set_property PACKAGE_PIN M17 		[get_ports ADC_PDWN]
#set_property IOSTANDARD LVCMOS25	[get_ports {ADC_PDWN}]
## frame clock
#set_property PACKAGE_PIN J20 [get_ports FCLK_P]
#set_property IOSTANDARD LVDS_25 [get_ports FCLK_P]
#set_property PACKAGE_PIN J21 [get_ports FCLK_N]
#set_property IOSTANDARD LVDS_25 [get_ports FCLK_N]
## data transfer clock
#set_property PACKAGE_PIN J19 [get_ports DCLK_P]
#set_property IOSTANDARD LVDS_25 [get_ports DCLK_P]
#set_property PACKAGE_PIN H19 [get_ports DCLK_N]
#set_property IOSTANDARD LVDS_25 [get_ports DCLK_N]
## 8 data output channels
#set_property PACKAGE_PIN H13 [get_ports A_P]
#set_property IOSTANDARD LVDS_25 [get_ports A_P]
#set_property PACKAGE_PIN G13 [get_ports A_N]
#set_property IOSTANDARD LVDS_25 [get_ports A_N]
#set_property PACKAGE_PIN G15 [get_ports B_P]
#set_property IOSTANDARD LVDS_25 [get_ports B_P]
#set_property PACKAGE_PIN G16 [get_ports B_N]
#set_property IOSTANDARD LVDS_25 [get_ports B_N]
#set_property PACKAGE_PIN N20 [get_ports C_P]
#set_property IOSTANDARD LVDS_25 [get_ports C_P]
#set_property PACKAGE_PIN M20 [get_ports C_N]
#set_property IOSTANDARD LVDS_25 [get_ports C_N]
#set_property PACKAGE_PIN G17 [get_ports D_P]
#set_property IOSTANDARD LVDS_25 [get_ports D_P]
#set_property PACKAGE_PIN G18 [get_ports D_N]
#set_property IOSTANDARD LVDS_25 [get_ports D_N]
#set_property PACKAGE_PIN J15 [get_ports E_P]
#set_property IOSTANDARD LVDS_25 [get_ports E_P]
#set_property PACKAGE_PIN H15 [get_ports E_N]
#set_property IOSTANDARD LVDS_25 [get_ports E_N]
#set_property PACKAGE_PIN H17 [get_ports F_P]
#set_property IOSTANDARD LVDS_25 [get_ports F_P]
#set_property PACKAGE_PIN H18 [get_ports F_N]
#set_property IOSTANDARD LVDS_25 [get_ports F_N]
#set_property PACKAGE_PIN J22 [get_ports G_P]
#set_property IOSTANDARD LVDS_25 [get_ports G_P]
#set_property PACKAGE_PIN H22 [get_ports G_N]
#set_property IOSTANDARD LVDS_25 [get_ports G_N]
#set_property PACKAGE_PIN H20 [get_ports H_P]
#set_property IOSTANDARD LVDS_25 [get_ports H_P]
#set_property PACKAGE_PIN G20 [get_ports H_N]
#set_property IOSTANDARD LVDS_25 [get_ports H_N]










create_debug_core u_ila_0 ila
set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_0]
set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_0]
set_property C_ADV_TRIGGER false [get_debug_cores u_ila_0]
set_property C_DATA_DEPTH 131072 [get_debug_cores u_ila_0]
set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_0]
set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_0]
set_property C_TRIGIN_EN false [get_debug_cores u_ila_0]
set_property C_TRIGOUT_EN false [get_debug_cores u_ila_0]
set_property port_width 1 [get_debug_ports u_ila_0/clk]
connect_debug_port u_ila_0/clk [get_nets [list clk_gen/inst/clk_out1]]
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe0]
set_property port_width 32 [get_debug_ports u_ila_0/probe0]
connect_debug_port u_ila_0/probe0 [get_nets [list {index[0]} {index[1]} {index[2]} {index[3]} {index[4]} {index[5]} {index[6]} {index[7]} {index[8]} {index[9]} {index[10]} {index[11]} {index[12]} {index[13]} {index[14]} {index[15]} {index[16]} {index[17]} {index[18]} {index[19]} {index[20]} {index[21]} {index[22]} {index[23]} {index[24]} {index[25]} {index[26]} {index[27]} {index[28]} {index[29]} {index[30]} {index[31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe1]
set_property port_width 6 [get_debug_ports u_ila_0/probe1]
connect_debug_port u_ila_0/probe1 [get_nets [list {i2c_fsm[0]} {i2c_fsm[1]} {i2c_fsm[2]} {i2c_fsm[3]} {i2c_fsm[4]} {i2c_fsm[5]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe2]
set_property port_width 8 [get_debug_ports u_ila_0/probe2]
connect_debug_port u_ila_0/probe2 [get_nets [list {i2c_data_rd[0]} {i2c_data_rd[1]} {i2c_data_rd[2]} {i2c_data_rd[3]} {i2c_data_rd[4]} {i2c_data_rd[5]} {i2c_data_rd[6]} {i2c_data_rd[7]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe3]
set_property port_width 1 [get_debug_ports u_ila_0/probe3]
connect_debug_port u_ila_0/probe3 [get_nets [list i2c_ack_error]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe4]
set_property port_width 1 [get_debug_ports u_ila_0/probe4]
connect_debug_port u_ila_0/probe4 [get_nets [list JC_busy_prev]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe5]
set_property port_width 1 [get_debug_ports u_ila_0/probe5]
connect_debug_port u_ila_0/probe5 [get_nets [list JC_clk_IN_158MHz]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe6]
set_property port_width 1 [get_debug_ports u_ila_0/probe6]
connect_debug_port u_ila_0/probe6 [get_nets [list JC_i2c_ack_error]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe7]
set_property port_width 1 [get_debug_ports u_ila_0/probe7]
connect_debug_port u_ila_0/probe7 [get_nets [list JC_i2c_busy]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe8]
set_property port_width 1 [get_debug_ports u_ila_0/probe8]
connect_debug_port u_ila_0/probe8 [get_nets [list JC_i2c_data_wr]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe9]
set_property port_width 1 [get_debug_ports u_ila_0/probe9]
connect_debug_port u_ila_0/probe9 [get_nets [list JC_i2c_ena_reg_n_0]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe10]
set_property port_width 1 [get_debug_ports u_ila_0/probe10]
connect_debug_port u_ila_0/probe10 [get_nets [list JC_scl_din]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe11]
set_property port_width 1 [get_debug_ports u_ila_0/probe11]
connect_debug_port u_ila_0/probe11 [get_nets [list Jc_scl_t]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe12]
set_property port_width 1 [get_debug_ports u_ila_0/probe12]
connect_debug_port u_ila_0/probe12 [get_nets [list JC_sda_t]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe13]
set_property port_width 1 [get_debug_ports u_ila_0/probe13]
connect_debug_port u_ila_0/probe13 [get_nets [list scl_din]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe14]
set_property port_width 1 [get_debug_ports u_ila_0/probe14]
connect_debug_port u_ila_0/probe14 [get_nets [list scl_t]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe15]
set_property port_width 1 [get_debug_ports u_ila_0/probe15]
connect_debug_port u_ila_0/probe15 [get_nets [list sda_din]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe16]
set_property port_width 1 [get_debug_ports u_ila_0/probe16]
connect_debug_port u_ila_0/probe16 [get_nets [list sda_t]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe17]
set_property port_width 1 [get_debug_ports u_ila_0/probe17]
connect_debug_port u_ila_0/probe17 [get_nets [list JC_sda_din]]
set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets CLK_I]
