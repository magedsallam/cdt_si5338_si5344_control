--------------------------------------------------------------------------------
--  ESS Detector Readout Project
--  Nauman Iqbal     RAL - 2019 
--
--
--
--  Description : Initialise Si570 to requested frequency
--
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;


library UNISIM;
use UNISIM.VComponents.all;

entity si5338_ctrl is
generic (
  I2C_INPUT_CLK_G : INTEGER := 100_000_000; -- Input clock [Hz]
  I2C_BUS_CLK_G   : INTEGER := 100_000;     -- I2C clock [Hz]
  I2C_THROTTLE_G  : INTEGER := 200_000;     -- Sysclk's between I2C pkts, 120_000
  ILA_DEBUG_ON_G  : string  := "false"
); 
port (
    -- system clock    
--    CLK50MHZ_P               : in    std_logic;
--    CLK50MHZ_N               : in    std_logic;
--    MGT_CLK2_P               : in    std_logic;
--    MGT_CLK2_N               : in    std_logic;
--    RESET_I                 : in    std_logic;
    -- I2C Interface
    sda                     : inout std_logic;
    scl                     : inout std_logic;

    MGT_CLK_P         : IN    std_logic;
    MGT_CLK_N         : IN    std_logic;
    CLK10MHz           :in    std_logic;
    LED_O                   : out   std_logic;
    
    --Jitter cleaner interface
    JC_in_clk_p       : out   std_logic;
    JC_in_clk_n       : out  std_logic;
    JC_sda            :inout std_logic;
    JC_scl            :inout std_logic;
    JC_reset_N        :out   std_logic;
    JC_A1             :out   std_logic;
    JC_out_clk_p        :in  std_logic;
    JC_out_clk_n         :in std_logic


);
end si5338_ctrl;

architecture rtl of si5338_ctrl is

type i2c_fsm_t is (INIT,disable_outputs,SYNC0,LOL,SYNC1,READ_INIT,SYNC2,READ,MASKING,SYNC3,WRITE,index_comp,wait_100ms,SYNC4,initiate_locking,wait_100ms2,SYNC5,restart_lol,wait_100ms3,read218_init,SYNC_read218,read218,SYNC6,use_FCAL,SYNC7,enable_outputs,si5338_FINISHED,JC_SYNC0,CONF_PRE_SI53xx,JC_SYNC1,DELAY300MS,JC_SYNC2,CONF_SI53xx,JC_SYNC3,CONF_POST_SI53xx,finished_5344);
type i2c_mem_t is array (natural range <>) of std_logic_vector(15 downto 0);
type i2c_data_t is array (natural range <>) of std_logic_vector(23 downto 0);
type i2c_data_t2 is array (natural range <>) of std_logic_vector(7 downto 0);


--------------------------------------------------------------------
-- Play Memory depth
constant si5344_CONF_SIZE          : integer := 467; -- number of registers in main config
constant si5344_PRE_CONF_SIZE      : integer := 5;  -- number of registers in Pre 300MS delay
constant si5344_POST_CONF_SIZE     : integer := 9;   -- number of registers in post main config
constant FIXED_DELAY        : integer := 30000000; -- 300MS dealy counter


-- Jitter cleaner memory map size
signal JC_i2c_mem          : i2c_mem_t(0 to si5344_CONF_SIZE -1);
signal JC_i2c_pre_mem      : i2c_mem_t(0 to si5344_PRE_CONF_SIZE-1);
signal JC_i2c_post_mem     : i2c_mem_t(0 to si5344_POST_CONF_SIZE-1);
 -- number of registers in main config
constant FMETER_CHANNELS    : integer := 5;
constant FMETER_NUM_BITS    : integer := 32;


signal si5338_regmap        : i2c_data_t(0 to 111);
signal index              : integer:=0; 
signal Masking_reg        : std_logic_vector (7 downto 0);
signal vio_start          : std_logic;
signal windex              :integer := 0;

signal CLK_I            : std_logic;
signal locked           : std_logic;
signal i2c_done         : std_logic;
signal bufgcounter      : integer:= 0;
signal CLK_158_bufg2    : std_logic;
signal CLK_IN_158MHz2   : std_logic;
signal masked_old_value : std_logic_vector(7 downto 0);
signal masked_new_value : std_logic_vector(7 downto 0);
signal obufds_158MHZ    : std_logic;
signal JC_clk_IN_158MHz : std_logic;
signal vio_start_JC     : std_logic;

signal clk_250MHz       : std_logic;
--signal CLK_158_bufggt   : std_logic;

signal i2c_fsm          : i2c_fsm_t;
signal next_state       : i2c_fsm_t;
signal busy_prev        : std_logic;
signal i2c_ena          : std_logic;
signal i2c_addr         : std_logic_vector(6 downto 0);
signal i2c_rw           : std_logic;
signal i2c_data_wr      : std_logic_vector(7 downto 0);
signal i2c_busy         : std_logic;
signal i2c_data_rd      : std_logic_vector(7 downto 0);
signal i2c_ack_error    : std_logic;
signal i2c_rise         : std_logic;
signal i2c_data         : i2c_data_t(7 downto 0);
signal i2c_start        : std_logic;
signal sda_din          : std_logic;
signal scl_din          : std_logic;
signal sda_t            : std_logic;
signal scl_t            : std_logic;
signal RESET_I          : std_logic:= '0';
signal CLK_IN_158MHz    : std_logic;
signal CLK_158_bufg     : std_logic;
signal CLK50MHZ         : std_logic;
signal maskreg_test     : std_logic_vector (7 downto 0);
signal waitcounter      : integer :=0; 

signal JC_i2c_ena          : std_logic;
signal JC_i2c_addr         : std_logic_vector(6 downto 0);
signal JC_i2c_rw           : std_logic;
signal JC_i2c_data_wr      : std_logic_vector(7 downto 0);
signal JC_i2c_busy         : std_logic;
signal JC_i2c_data_rd      : std_logic_vector(7 downto 0);
signal JC_i2c_ack_error    : std_logic;
signal JC_i2c_rise         : std_logic;
signal JC_i2c_data         : i2c_data_t(7 downto 0);
signal JC_i2c_start        : std_logic;
signal JC_sda_din          : std_logic;
signal JC_scl_din          : std_logic;
signal JC_sda_t            : std_logic;
signal Jc_scl_t            : std_logic;
signal JC_busy_prev        : std_logic;

signal i2c_play         : std_logic;



signal fin              : std_logic_vector(FMETER_CHANNELS-1 downto 0);
signal fout0            : std_logic_vector(FMETER_NUM_BITS-1 downto 0);
signal fout1            : std_logic_vector(FMETER_NUM_BITS-1 downto 0);
signal fout2            : std_logic_vector(FMETER_NUM_BITS-1 downto 0);
signal fout3            : std_logic_vector (FMETER_NUM_BITS-1 downto 0);
signal fout4            : std_logic_vector (FMETER_NUM_BITS-1 downto 0);
signal update           : std_logic_vector(0 downto 0);


attribute mark_debug     : string;
constant  dbg_state      : string := "true";
attribute mark_debug of sda_din : signal is dbg_state;
attribute mark_debug of scl_din : signal is dbg_state;
attribute mark_debug of sda_t   : signal is dbg_state;
attribute mark_debug of scl_t   : signal is dbg_state;

attribute mark_debug of i2c_fsm : signal is dbg_state;
attribute mark_debug of i2c_data_rd    : signal is dbg_state;
attribute mark_debug of i2c_ack_error  : signal is dbg_state;
attribute mark_debug of JC_i2c_ack_error  : signal is dbg_state;
attribute mark_debug of index : signal is dbg_state;



component clk_wiz_0
port
 (-- Clock in ports
  -- Clock out ports
  clk_out1          : out    std_logic;


  -- Status and control signals
  reset             : in     std_logic;
  locked            : out    std_logic;
  clk_in1          : in     std_logic


 );
end component;

COMPONENT vio_freq_meter
  PORT (
    clk : IN STD_LOGIC;
    probe_in0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe_in1 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe_in2 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe_in3 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe_in4 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe_in5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
  );
END COMPONENT;

COMPONENT vio_0
  PORT (
    clk : IN STD_LOGIC;
    probe_out0 : out STD_LOGIC;
    probe_out1 : out STD_LOGIC
    
  );
END COMPONENT;



begin

--Jitter cleaner preconfiguration memory map
JC_i2c_pre_mem <=( 

X"0B01",
X"C024",
X"0025",
X"0501",
X"0140");

--Jitter cleaner main configuration memory map
JC_i2c_mem  <=(
X"0001",
X"0006",
X"0007",
X"0008",
X"680B",
X"0216",
X"DC17",
X"E618",
X"DD19",
X"DF1A",
X"022B",
X"092C",
X"412D",
X"3A2E",
X"002F",
X"0030",
X"0031",
X"0032",
X"0033",
X"3A34",
X"0035",
X"3A36",
X"0037",
X"0038",
X"0039",
X"003A",
X"003B",
X"3A3C",
X"003D",
X"113F",
X"0040",
X"0E41",
X"0042",
X"0043",
X"0E44",
X"0C45",
X"3246",
X"0047",
X"0048",
X"0049",
X"324A",
X"004B",
X"004C",
X"004D",
X"054E",
X"004F",
X"0750",
X"0351",
X"0052",
X"0053",
X"0054",
X"0355",
X"0056",
X"0057",
X"0058",
X"0159",
X"005A",
X"005B",
X"005C",
X"015D",
X"005E",
X"005F",
X"0060",
X"0061",
X"0062",
X"0063",
X"0064",
X"0065",
X"0066",
X"0067",
X"0068",
X"0069",
X"0292",
X"A093",
X"0095",
X"8096",
X"6098",
X"029A",
X"609B",
X"089D",
X"409E",
X"20A0",
X"00A2",
X"84A9",
X"61AA",
X"00AB",
X"00AC",
X"00E5",
X"0AEA",
X"60EB",
X"00EC",
X"00ED",
X"0101",
X"0102",
X"0612",
X"0913",
X"3B14",
X"2815",
X"0117",
X"0918",
X"3B19",
X"281A",
X"0126",
X"0927",
X"3B28",
X"2829",
X"062B",
X"092C",
X"3B2D",
X"282E",
X"003F",
X"0140",
X"4041",
X"FF42",
X"0201",
X"0006",
X"5008",
X"0009",
X"000A",
X"000B",
X"000C",
X"000D",
X"010E",
X"000F",
X"0010",
X"0011",
X"0012",
X"0013",
X"0014",
X"0015",
X"0016",
X"0017",
X"0018",
X"0019",
X"001A",
X"001B",
X"001C",
X"001D",
X"001E",
X"001F",
X"0020",
X"0021",
X"0022",
X"0023",
X"0024",
X"0025",
X"5026",
X"0027",
X"0028",
X"0029",
X"002A",
X"002B",
X"012C",
X"002D",
X"002E",
X"002F",
X"0B31",
X"0B32",
X"0B33",
X"0B34",
X"0035",
X"0036",
X"8037",
X"5F38",
X"CE39",
X"003A",
X"003B",
X"003C",
X"003D",
X"C83E",
X"0050",
X"0051",
X"0052",
X"0053",
X"0054",
X"0055",
X"005C",
X"005D",
X"005E",
X"005F",
X"0060",
X"0061",
X"456B",
X"536C",
X"536D",
X"5F6E",
X"736F",
X"7970",
X"6E71",
X"6372",
X"008A",
X"008B",
X"008C",
X"008D",
X"008E",
X"008F",
X"0090",
X"0091",
X"B094",
X"0296",
X"0297",
X"0299",
X"FA9D",
X"019E",
X"009F",
X"CCA9",
X"04AA",
X"00AB",
X"FFB7",
X"0301",
X"0002",
X"0003",
X"0004",
X"8005",
X"1606",
X"0007",
X"0008",
X"0009",
X"000A",
X"800B",
X"000C",
X"000D",
X"000E",
X"000F",
X"0010",
X"0011",
X"0012",
X"0013",
X"0014",
X"0015",
X"0016",
X"0017",
X"0018",
X"0019",
X"001A",
X"001B",
X"001C",
X"001D",
X"001E",
X"001F",
X"0020",
X"0021",
X"0022",
X"0023",
X"0024",
X"0025",
X"0026",
X"0027",
X"0028",
X"0029",
X"002A",
X"002B",
X"002C",
X"002D",
X"0038",
X"1F39",
X"003B",
X"003C",
X"003D",
X"003E",
X"003F",
X"0040",
X"0041",
X"0042",
X"0043",
X"0044",
X"0045",
X"0046",
X"0047",
X"0048",
X"0049",
X"004A",
X"004B",
X"004C",
X"004D",
X"004E",
X"004F",
X"0050",
X"0051",
X"0052",
X"0059",
X"005A",
X"005B",
X"005C",
X"005D",
X"005E",
X"005F",
X"0060",
X"0401",
X"0187",
X"0501",
X"1008",
X"1F09",
X"0C0A",
X"0B0B",
X"3F0C",
X"3F0D",
X"130E",
X"270F",
X"0910",
X"0811",
X"3F12",
X"3F13",
X"0015",
X"0016",
X"0017",
X"0018",
X"D019",
X"021A",
X"001B",
X"001C",
X"001D",
X"001E",
X"801F",
X"2B21",
X"012A",
X"012B",
X"872C",
X"032D",
X"192E",
X"192F",
X"0031",
X"4A32",
X"0333",
X"0034",
X"0035",
X"0036",
X"0037",
X"0038",
X"0039",
X"023A",
X"033B",
X"003C",
X"113D",
X"063E",
X"0D89",
X"008A",
X"F89B",
X"109D",
X"219E",
X"0C9F",
X"0BA0",
X"3FA1",
X"3FA2",
X"03A6",
X"0801",
X"3502",
X"0503",
X"0004",
X"0005",
X"0006",
X"0007",
X"0008",
X"0009",
X"000A",
X"000B",
X"000C",
X"000D",
X"000E",
X"000F",
X"0010",
X"0011",
X"0012",
X"0013",
X"0014",
X"0015",
X"0016",
X"0017",
X"0018",
X"0019",
X"001A",
X"001B",
X"001C",
X"001D",
X"001E",
X"001F",
X"0020",
X"0021",
X"0022",
X"0023",
X"0024",
X"0025",
X"0026",
X"0027",
X"0028",
X"0029",
X"002A",
X"002B",
X"002C",
X"002D",
X"002E",
X"002F",
X"0030",
X"0031",
X"0032",
X"0033",
X"0034",
X"0035",
X"0036",
X"0037",
X"0038",
X"0039",
X"003A",
X"003B",
X"003C",
X"003D",
X"003E",
X"003F",
X"0040",
X"0041",
X"0042",
X"0043",
X"0044",
X"0045",
X"0046",
X"0047",
X"0048",
X"0049",
X"004A",
X"004B",
X"004C",
X"004D",
X"004E",
X"004F",
X"0050",
X"0051",
X"0052",
X"0053",
X"0054",
X"0055",
X"0056",
X"0057",
X"0058",
X"0059",
X"005A",
X"005B",
X"005C",
X"005D",
X"005E",
X"005F",
X"0060",
X"0061",
X"0901",
X"020E",
X"0043",
X"0949",
X"094A",
X"494E",
X"024F",
X"005E",
X"0A01",
X"0002",
X"0103",
X"0104",
X"0105",
X"0014",
X"001A",
X"0020",
X"0026",
X"0B01",
X"2F44",
X"0046",
X"0647",
X"0648",
X"0E4A",
X"F057",
X"0058");   

-- Jitter cleaner post configuration memory map
JC_i2c_post_mem <=(

X"0501",
X"0114",
X"0001",
X"011C",
X"0501",
X"0040",
X"0B01",
X"C324",
X"0225"
); 

-- Register map for setting si5338 output 2 to 158.4945MHz  
si5338_regmap<=
(
x"34107F",
x"1F001C",
x"20001C",
x"21001C",
x"22001C",
x"4000FF",
x"4117FF",
x"4200FF",
x"4300FF",
x"4400FF",
x"4500FF",
x"4601FF",
x"4700FF",
x"4800FF",
x"49003F",
x"4A107F",
x"4BE2FF",
x"4C05FF",
x"4D98FF",
x"4E4CFF",
x"4F13FF",
x"5000FF",
x"513DFF",
x"52D6FF",
x"5304FF",
x"54003F",
x"5600FF",
x"5717FF",
x"5800FF",
x"5900FF",
x"5A00FF",
x"5B00FF",
x"5C01FF",
x"5D00FF",
x"5E00FF",
x"5F003F",
x"6100FF",
x"6230FF",
x"6300FF",
x"6400FF",
x"6500FF",
x"6600FF",
x"6701FF",
x"6800FF",
x"6900FF",
x"6A80BF",
x"9800FF",
x"9900FF",
x"9A00FF",
x"9B00FF",
x"9C00FF",
x"9D00FF",
x"9E000F",
x"9F000F",
x"A000FF",
x"A100FF",
x"A200FF",
x"A300FF",
x"A400FF",
x"A500FF",
x"A600FF",
x"A700FF",
x"A800FF",
x"A900FF",
x"AA00FF",
x"AB00FF",
x"AC00FF",
x"AD00FF",
x"AE00FF",
x"AF00FF",
x"B000FF",
x"B100FF",
x"B200FF",
x"B300FF",
x"B400FF",
x"B5000F",
x"B600FF",
x"B700FF",
x"B800FF",
x"B900FF",
x"BA00FF",
x"BB00FF",
x"BC00FF",
x"BD00FF",
x"BE00FF",
x"BF00FF",
x"C000FF",
x"C100FF",
x"C200FF",
x"C300FF",
x"C400FF",
x"C500FF",
x"C600FF",
x"C700FF",
x"C800FF",
x"C900FF",
x"CA00FF",
x"CB000F",
x"CC00FF",
x"CD00FF",
x"CE00FF",
x"CF00FF",
x"D000FF",
x"D100FF",
x"D200FF",
x"D300FF",
x"D400FF",
x"D500FF",
x"D600FF",
x"D700FF",
x"D800FF",
x"D900FF"
);

 

------------------------------------------------------------------------------
clk_gen : clk_wiz_0
   port map ( 
        -- Clock out ports  
        clk_out1 => CLK_I,
        -- Status and control signals                
        reset => RESET_I,
        locked => locked,
        -- Clock in ports
        clk_in1 => CLK10MHz
  

 );

   -- Receiving  si5338 output clock
    IBUFDS_GTE2_inst_si5338: IBUFDS_GTE2
generic map(
CLKRCV_TRST =>TRUE,
CLKCM_CFG => TRUE,
CLKSWING_CFG =>"11"
)
port map (
    O               => CLK_IN_158MHz,
    I               => MGT_CLK_P,
    IB              => MGT_CLK_N,
    CEB             => '0',
    ODIV2           => open
);

 -- Receiving  si5344 output clock
    IBUFDS_GTE2_inst_si5344: IBUFDS_GTE2
generic map(
CLKRCV_TRST =>TRUE,
CLKCM_CFG => TRUE,
CLKSWING_CFG =>"11"
)
port map (
    O               => JC_clk_IN_158MHz,
    I               => JC_out_clk_p,
    IB              => JC_out_clk_n,
    CEB             => '0',
    ODIV2           => open
);


-- clock output from fabric into si5344 to be jitter cleaned
OBUFDS_inst : OBUFDS
generic map (
IOSTANDARD => "DEFAULT", -- Specify the output I/O standard
SLEW => "SLOW") -- Specify the output slew rate
port map (
O => JC_in_clk_p , -- Diff_p output (connect directly to top-level port)
OB => JC_in_clk_n , -- Diff_n output (connect directly to top-level port)
I => obufds_158MHZ -- Buffer input
);

ODDR_inst : ODDR
generic map(
DDR_CLK_EDGE => "OPPOSITE_EDGE", -- "OPPOSITE_EDGE" or "SAME_EDGE"
INIT => '0', -- Initial value for Q port ('1' or '0')
SRTYPE => "SYNC") -- Reset Type ("ASYNC" or "SYNC")
port map (
Q => obufds_158MHZ, -- 1-bit DDR output
C =>CLK_IN_158MHz, -- 1-bit clock input
CE => '1', -- 1-bit clock enable input
D1 => '1', -- 1-bit data input (positive edge)
D2 => '0', -- 1-bit data input (negative edge)
R => '0', -- 1-bit reset input
S => '0' -- 1-bit set input
);
-- End of OBUFDS_inst instantiation
blinky_1 : entity work.blinky
    Generic map (
        COUNTVAL => 25 ) 
    Port map (
        CLK_I   => clk_IN_158MHz,
        LED_O   => LED_O 
    );
    

    
-----------------------------------------------------------------
-- Frequency meter
-----------------------------------------------------------------
fin(0) <= CLK_I;
fin(1) <= JC_clk_IN_158MHz;
fin(2) <= CLK10MHz;
fin(3) <= '0';--obufds_158MHZ;
fin(4) <= CLK_IN_158MHz;

fmeter : entity work.labtools_fmeter
    generic map (
        C_REFCLK_HZ => 100000000,
        C_CHANNELS  => FMETER_CHANNELS,
        C_MODE      => 0,
        C_NUM_BITS  => FMETER_NUM_BITS
    )  
    port map ( 
        refclk => CLK_I,
        fin    => fin,
        
        F0    => fout0(FMETER_NUM_BITS -1 downto 0),
        F1    => fout1(FMETER_NUM_BITS -1 downto 0),
        F2    => fout2(FMETER_NUM_BITS -1 downto 0),
        F3    => fout3(FMETER_NUM_BITS -1 downto 0),
        F4    => fout4(FMETER_NUM_BITS -1 downto 0),
        F5    => open,
        F6    => open,
        F7    => open,            
        F8    => open,
        F9    => open,
        F10   => open,
        F11   => open,
        F12   => open,
        F13   => open,
        F14   => open,
        F15   => open,            
        F16   => open,
        F17   => open,
        F18   => open,
        F19   => open,            
        F20   => open,
        F21   => open,
        F22   => open,
        F23   => open,            
        F24   => open,
        F25   => open,
        F26   => open,
        F27   => open,
        F28   => open,
        F29   => open,
        F30   => open,
        F31   => open,            
        update =>  update(0)
    );
    
   vi0_start: vio_0
  port map (
  clk=> CLK_I,
  probe_out0=> vio_start,
  probe_out1=> vio_start_JC

   );

vio_ip : vio_freq_meter
    PORT MAP (
        clk       => CLK_I,
        probe_in0 => fout0,
        probe_in1 => fout1,
        probe_in2 => fout2,
        probe_in3 => fout3,
        probe_in4 => fout4,
        probe_in5 => update(0 downto 0)
    );

--------------------------------------------------------------------
-- 3-state I2C I/O Buffers
--------------------------------------------------------------------
iobuf_scl : iobuf
port map (
    I  => '0',
    O  => scl_din,
    IO => scl,
    T  => scl_t
);

iobuf_sda : iobuf
port map (
    I  => '0',
    O  => sda_din,
    IO => sda,
    T  => sda_t
);
iobuf_JC_scl : iobuf
port map (
    I  => '0',
    O  => JC_scl_din,
    IO => JC_scl,
    T  => JC_scl_t
);

iobuf_JC_sda : iobuf
port map (
    I  => '0',
    O  => JC_sda_din,
    IO => JC_sda,
    T  => JC_sda_t
);

--------------------------------------------------------------------
-- I2C Master Device  for si5338
--------------------------------------------------------------------
i2c_master_inst : entity work.i2c_master
generic map (
    input_clk   => I2C_INPUT_CLK_G,
    bus_clk     => I2C_BUS_CLK_G
)
port map (
    clk         => CLK_I,
    reset       => RESET_I,
    ena         => i2c_ena,
    addr        => i2c_addr,
    rw          => i2c_rw,
    data_wr     => i2c_data_wr,
    busy        => i2c_busy,
    data_rd     => i2c_data_rd,
    ack_error   => i2c_ack_error,
    sda         => sda_din,
    scl         => scl_din,
    sda_t       => sda_t,
    scl_t       => scl_t
);
--------------------------------------------------------------------
-- I2C Master Device  for si5344
--------------------------------------------------------------------
JCi2c_master_inst : entity work.i2c_master
generic map (
    input_clk   => I2C_INPUT_CLK_G,
    bus_clk     => I2C_BUS_CLK_G
)
port map (
    clk         => CLK_I,
    reset       => RESET_I,
    ena         => JC_i2c_ena,
    addr        => JC_i2c_addr,
    rw          => JC_i2c_rw,
    data_wr     => JC_i2c_data_wr,
    busy        => JC_i2c_busy,
    data_rd     => JC_i2c_data_rd,
    ack_error   => JC_i2c_ack_error,
    sda         => JC_sda_din,
    scl         => JC_scl_din,
    sda_t       => JC_sda_t,
    scl_t       => JC_scl_t
);


-- Throttle down I2C packets just to be on the safe side
start_presc : entity work.prescaler
generic map (
    I2C_THROTTLE_G  => I2C_THROTTLE_G     -- Sysclk's between I2C pkts, 120_000;     -- Sysclk's between I2C pkts, 120_000
    )
port map (
    clk_i       => CLK_I,
    reset_i     => RESET_I,
    pulse_o     => i2c_start
);

--------------------------------------------------------------------
-- Main read state machine loops through all SLAVES
--------------------------------------------------------------------
i2c_rise <= i2c_busy and not busy_prev;
JC_i2c_rise <= JC_i2c_busy and not JC_busy_prev;


process(clk_i)
    variable busy_cnt         : natural range 0 to 15;
    variable count_delay      : natural range 0 to 30000000;
begin
    if rising_edge(clk_i) then
        if (reset_i = '1') then
            i2c_fsm     <= INIT;

            busy_cnt    := 0;
            busy_prev   <= '1';
            i2c_rw      <= '1';
            i2c_ena     <= '0';
            i2c_addr    <= (others => '0');
            i2c_data_wr <= X"00";
            JC_busy_prev   <= '1';
            JC_i2c_rw      <= '1';
            JC_i2c_ena     <= '0';
            JC_i2c_addr    <= (others => '0');
            JC_i2c_data_wr <= X"00";
--            I2C_DONE_O  <= '0';
        else
            busy_prev <= i2c_busy;
            JC_busy_prev<= JC_i2c_busy;
            case (i2c_fsm) is
                -- Wait I2C master to start
                when INIT =>  --  Si5338 is programmed first  triggered  by vio_start
                    if (i2c_busy = '0' and i2c_start = '1' and vio_start = '1') then
                        i2c_fsm        <= disable_outputs;
       
                    end if;
                    
                    
                  when disable_outputs =>
                      if (i2c_rise = '1') then
                        busy_cnt := busy_cnt + 1;
                      end if;

                    case busy_cnt is
                        -- Latch slave address and register address
                        when 0 =>
                            i2c_ena     <= '1';
                            i2c_addr    <= "1110000";  -- I2C Addr of si5338 = 0x70
                            i2c_rw      <= '0';          -- write
                            i2c_data_wr <= "11100110" ;  -- register address

                        -- Latch register data    
                        when 1 =>
                            i2c_data_wr <= "00010000";  -- register value

                        -- Continue until all values are written
                        when 2 =>
                            i2c_ena <= '0';
                            if (i2c_busy = '0') then
                                busy_cnt := 0;
                                i2c_fsm<= SYNC0;
                                
                            end if;

                         when others => NULL;
                        end case;
                        
                        when SYNC0 =>
                          if (i2c_busy = '0' and i2c_start = '1') then
                               i2c_fsm <= LOL;
                           end if;
                           
                   when LOL =>
                    if (i2c_rise = '1') then
                        busy_cnt := busy_cnt + 1;
                    end if;

                    case busy_cnt is
                        -- Latch slave address and register address
                        when 0 =>
                            i2c_ena     <= '1';
                            i2c_addr    <= "1110000";  -- I2C Addr 
                            i2c_rw      <= '0';          -- write
                            i2c_data_wr <= "11110001" ;  -- reg addr

                        -- Latch register data    
                        when 1 =>
                            i2c_data_wr <= "11100101"; -- reg new val

                        -- Continue until all values are written
                        when 2 =>
                            i2c_ena <= '0';
                            if (i2c_busy = '0') then
                                busy_cnt := 0;
                                i2c_fsm<=SYNC1;
                                
                            end if;

                         when others => NULL;
                        end case;
                        
                        when SYNC1 =>
                          if (i2c_busy = '0' and i2c_start = '1') then
                               i2c_fsm <= read_init;
                               
                           end if;
                           
                when Read_init =>  -- Read-modify- write process of Register map  using given register masks
                     if (i2c_rise = '1') then
                        busy_cnt := busy_cnt + 1;
                    end if;
            
                    case busy_cnt is
                        when 0 =>
                            i2c_ena <= '1';
                            i2c_addr <= "1110000";  --  Address of S5338
                            i2c_rw <= '0';          -- Write 
                            i2c_data_wr <= si5338_regmap(index)(23 downto 16);   --address of vreg to be updated
                        when 1 =>            
                            i2c_ena <= '0';
                            if (i2c_busy = '0') then
                                busy_cnt := 0;
                                i2c_fsm <= SYNC2;
                            end if;
                        when others => NULL;
                      end case;
                            
                      when SYNC2 =>
                          if (i2c_busy = '0' and i2c_start = '1') then
                               i2c_fsm <= read;
                           end if;
                           
                   when Read=>
                     if (i2c_rise = '1') then
                        busy_cnt := busy_cnt + 1;
                    end if;
            
                    case busy_cnt is
                        when 0 =>
                            i2c_ena <= '1';
                            i2c_addr <= "1110000";  --  Address of S5338
                            i2c_rw <= '1';          -- read

                        when 1 =>            
                            i2c_ena <= '0';
                            if (i2c_busy = '0') then
                                busy_cnt := 0;
                                i2c_fsm <= Masking;
                              
                           
                            end if;
                        when others => NULL;
                      end case;
      
                     When MASKING=>
                     
                         masked_old_value <= i2c_data_rd and not si5338_regmap(index)(7 downto 0);
                         masked_new_value <=  si5338_regmap(index)(15 downto 8) and si5338_regmap(index)(7 downto 0) ;                       
                         i2c_fsm <= SYNC3;

                        
                     when SYNC3 =>
                          if (i2c_busy = '0' and i2c_start = '1') then
                               i2c_fsm <= Write;
                           end if;
                           
                    when Write =>
                    if (i2c_rise = '1') then
                        busy_cnt := busy_cnt + 1;
                    end if;

                    case busy_cnt is
                        -- Latch slave address and register address
                        when 0 =>
                            i2c_ena     <= '1';
                            i2c_addr    <= "1110000";  -- 
                            i2c_rw      <= '0';          -- write
                            i2c_data_wr <= si5338_regmap(index)(23 downto 16) ;  -- writing registers

                        -- Latch register data    
                        when 1 =>
                            i2c_data_wr <=  masked_old_value or  masked_new_value;

                        -- Continue until all values are written
                        when 2 =>
                            i2c_ena <= '0';
                            if (i2c_busy = '0') then
                                busy_cnt := 0;
                                i2c_fsm<=index_comp;
                                
                            end if;

                         when others => NULL;
                        end case;
                        
                      When index_comp =>
                       if index < 111 then   -- when regmap is done go next step of programming the chip
                        index <= index + 1 ;
                        i2c_fsm<= SYNC1;
                        else
                        index <= 0;
                        i2c_fsm<= wait_100ms;
                       end if;
                      
                      when wait_100ms =>
                      
                      if waitcounter = 1000000 then
                        i2c_fsm <= SYNC4;
                        waitcounter<=0;
                      else
                        waitcounter <= waitcounter +1; 
                      end if;
                      
                      when SYNC4 =>
                        if (i2c_busy = '0' and i2c_start = '1') then
                               i2c_fsm <= initiate_locking;
                           end if;
                           

                      when initiate_locking =>
                                      
                           if (i2c_rise = '1') then
                             busy_cnt := busy_cnt + 1;
                            end if;

                       case busy_cnt is
                        -- Latch slave address and register address
                        when 0 =>
                            i2c_ena     <= '1';
                            i2c_addr    <= "1110000";  -- I2C Addr 
                            i2c_rw      <= '0';          -- write
                            i2c_data_wr <= x"F6" ;  -- writing registers

                        -- Latch register data    
                        when 1 =>
                            i2c_data_wr <= x"02";

                      
                        when 2 =>
                            i2c_ena <= '0';
                            if (i2c_busy = '0') then
                                busy_cnt := 0;
                                i2c_fsm<=wait_100ms2;
                            end if;

                            when others => NULL;
                         end case;

                     when wait_100ms2 =>
                      
                      if waitcounter = 1000000 then
                        i2c_fsm <= SYNC5;
                        waitcounter<=0;
                      else
                        waitcounter <= waitcounter +1; 
                      end if;
                      
                      
                       when SYNC5 =>
                        if (i2c_busy = '0' and i2c_start = '1') then
                               i2c_fsm <= restart_lol;
                           end if;
                           
                           
                        when restart_lol =>
                                      
                           if (i2c_rise = '1') then
                             busy_cnt := busy_cnt + 1;
                            end if;

                       case busy_cnt is
                        -- Latch slave address and register address
                        when 0 =>
                            i2c_ena     <= '1';
                            i2c_addr    <= "1110000";  --
                            i2c_rw      <= '0';          -- write
                            i2c_data_wr <= x"F1" ;  -- writing registers

                        -- Latch register data    
                        when 1 =>
                            i2c_data_wr <= x"65";

                        -- Continue 
                        when 2 =>
                            i2c_ena <= '0';
                            if (i2c_busy = '0') then
                                busy_cnt := 0;
                                i2c_fsm<=wait_100ms3;
                            end if;

                            when others => NULL;
                         end case;
                         
                         
                    when wait_100ms3 =>
                       JC_reset_N <='0';
                      if waitcounter = 1000000 then
                        i2c_fsm <= SYNC6;
                        waitcounter<=0;
                      else
                        waitcounter <= waitcounter +1; 
                      end if;
                      
                        when SYNC6 =>
                        if (i2c_busy = '0' and i2c_start = '1') then
                               i2c_fsm <= read218_init;
                           end if;
                           
                 when read218_init =>  -- this is used for  status monitoring ( if return value is not 0) something is wrong
                      
                                 
                     if (i2c_rise = '1') then
                        busy_cnt := busy_cnt + 1;
                    end if;
            
                    case busy_cnt is
                        when 0 =>
                            i2c_ena <= '1';
                            i2c_addr <= "1110000";  --  Address of S5338
                            i2c_rw <= '0';          -- Write cmd
                            i2c_data_wr <= x"EB";   -- value of register to be read
                        when 1 =>            
                            i2c_ena <= '0';
                            if (i2c_busy = '0') then
                                busy_cnt := 0;
                                i2c_fsm <= SYNC_read218;
                            end if;
                        when others => NULL;
                      end case;
                            
                      when SYNC_read218 =>
                          if (i2c_busy = '0' and i2c_start = '1') then
                               i2c_fsm <= read218;
                           end if;
                           
                       when read218 =>      -- this is used for chipscope  status monitoring ( if return value is not 0) something is wrong
                             if (i2c_rise = '1') then
                        busy_cnt := busy_cnt + 1;
                    end if;
            
                    case busy_cnt is
                        when 0 =>
                            i2c_ena <= '1';
                            i2c_addr <= "1110000";     --  Address of S5338
                            i2c_rw <= '1';          -- read

                        when 1 =>            
                            i2c_ena <= '0';
                            if (i2c_busy = '0') then
                                busy_cnt := 0;
                                i2c_fsm <= use_FCAL;
                           
                            end if;
                        when others => NULL;
                      end case;
                           
                           
                        when use_FCAL=>   
                               
                           if (i2c_rise = '1') then
                             busy_cnt := busy_cnt + 1;
                            end if;

                       case busy_cnt is
                        -- Latch slave address and register address
                        when 0 =>
                            i2c_ena     <= '1';
                            i2c_addr    <= "1110000";  -- I2C Addr of si5338
                            i2c_rw      <= '0';          -- write
                            i2c_data_wr <= x"31" ;  -- writing registers

                        -- Latch register data    
                        when 1 =>
                            i2c_data_wr <= x"80";

             
                        when 2 =>
                            i2c_ena <= '0';
                            if (i2c_busy = '0') then
                                busy_cnt := 0;
                                i2c_fsm<=SYNC7;
                            end if;

                            when others => NULL;
                         end case;
                         
                           when SYNC7 =>
                        if (i2c_busy = '0' and i2c_start = '1') then
                               i2c_fsm <= enable_outputs;
                         
                            JC_reset_N <='1';
                           end if;

                           
                        when enable_outputs=>
                        
                                   
                           if (i2c_rise = '1') then
                             busy_cnt := busy_cnt + 1;
                            end if;

                       case busy_cnt is
                        -- Latch slave address and register address
                        when 0 =>
                            i2c_ena     <= '1';
                            i2c_addr    <= "1110000";  -- I2C Addr of si570 = 0x55
                            i2c_rw      <= '0';          -- write
                            i2c_data_wr <= x"E6" ;  -- writing registers

                        -- Latch register data    
                        when 1 =>
                            i2c_data_wr <= x"00";

                    
                        when 2 =>
                            i2c_ena <= '0';
                            if (i2c_busy = '0') then
                                busy_cnt := 0;
                                i2c_fsm<=si5338_FINISHED;
                            end if;

                            when others => NULL;
                         end case;
                            
                       when si5338_FINISHED=>
                      
                        if  vio_start_JC = '1'  then   -- start programming si5344 triggered by vio_start_JC
                         i2c_fsm<=JC_SYNC0;
                        end if ;
                        
                  
             --    end case;
                 
                  when JC_SYNC0 => 
                    if (JC_i2c_busy = '0' and i2c_start = '1') then
                        i2c_fsm <= CONF_PRE_SI53xx;
                    end if;
                     
                when CONF_PRE_SI53xx =>
                    if (JC_i2c_rise = '1') then
                        busy_cnt := busy_cnt + 1;
                    end if;
                    case busy_cnt is
                        -- Latch slave address and register address
                        when 0 =>
                            JC_i2c_ena     <= '1';
                            JC_i2c_addr    <= "1101000";  -- I2C Addr of si5344 = 0x68
                            JC_i2c_rw      <= '0';          -- write
                            JC_i2c_data_wr <= JC_i2c_pre_mem(windex)(7 downto 0);  -- writing registers

                        -- Latch register data    
                        when 1 =>
                            JC_i2c_data_wr <= JC_i2c_pre_mem(windex)(15 downto 8);

                        -- Continue until all values are written
                        when 2 =>
                            JC_i2c_ena <= '0';
                            if (JC_i2c_busy = '0') then
                                busy_cnt := 0;
                                if (windex = si5344_PRE_CONF_SIZE - 1) then
                                    i2c_fsm <= JC_SYNC1;
                                else
                                    windex  <= windex + 1;    
                                    i2c_fsm <= JC_SYNC0;
                                end if;
                            end if;

                        when others => NULL;
                    end case;
                when JC_SYNC1 =>
                    if (i2c_start = '1') then
                        i2c_fsm <= DELAY300MS;
                    end if;
                when DELAY300MS =>
                    if (count_delay = FIXED_DELAY) then
                        windex  <= 0;
                        i2c_fsm <= JC_SYNC2;
                    else
                        count_delay := count_delay + 1;
                    end if;
                when JC_SYNC2 =>
                    if (JC_i2c_busy = '0' and i2c_start = '1') then
                        i2c_fsm <= CONF_SI53xx;
                    end if;    
                -- Write to Si53xx configuration registers
                when CONF_SI53xx =>
                    if (JC_i2c_rise = '1') then
                        busy_cnt := busy_cnt + 1;
                    end if;

                    case busy_cnt is
                        -- Latch slave address and register address
                        when 0 =>
                            JC_i2c_ena     <= '1';
                            JC_i2c_addr    <= "1101000";  
                            JC_i2c_rw      <= '0';          -- write
                            JC_i2c_data_wr <= JC_i2c_mem(windex)(7 downto 0);  -- writing registers

                        -- Latch register data    
                        when 1 =>
                            JC_i2c_data_wr <= JC_i2c_mem(windex)(15 downto 8);

                        -- Continue until all values are written
                        when 2 =>
                            JC_i2c_ena <= '0';
                            if (JC_i2c_busy = '0') then
                                busy_cnt := 0;
                                if (windex = si5344_CONF_SIZE - 1) then
                                    windex  <= 0;
--                                
                                    i2c_fsm <= JC_SYNC3;
                                else
                                    windex  <= windex + 1;    
                                    i2c_fsm <= JC_SYNC2; 
                                end if;
                            end if;

                        when others => NULL;
                    end case;
                when JC_SYNC3 =>
                    if (JC_i2c_busy = '0' and i2c_start = '1') then
                        i2c_fsm <= CONF_POST_SI53xx;
                    end if;
                when CONF_POST_SI53xx =>
                    if (JC_i2c_rise = '1') then
                        busy_cnt := busy_cnt + 1;
                    end if;
        
                    case busy_cnt is
                        -- Latch slave address and register address
                        when 0 =>
                            JC_i2c_ena     <= '1';
                            JC_i2c_addr    <= "1101000";  --
                            JC_i2c_rw      <= '0';          -- write
                            JC_i2c_data_wr <= JC_i2c_post_mem(windex)(7 downto 0);  -- writing registers
        
                        -- Latch register data    
                        when 1 =>
                            JC_i2c_data_wr <= JC_i2c_post_mem(windex)(15 downto 8);
        
                        -- Continue until all values are written
                        when 2 =>
                            JC_i2c_ena <= '0';
                            if (JC_i2c_busy = '0') then
                                busy_cnt := 0;
                                if (windex = si5344_POST_CONF_SIZE - 1) then
                                    i2c_fsm <= finished_5344;
                                else
                                    windex  <= windex + 1;    
                                    i2c_fsm <= JC_SYNC3; 
                                end if;
                            end if;
        
                        when others => NULL;
                    end case;
                    
                    when finished_5344 =>
                        i2c_done <='1';
                    
                    when others => Null;
                    
                  end case;
        end if;
end if;
end process;

RESET_I<='0';
JC_A1 <='0';            

end rtl;
